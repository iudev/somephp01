<?php
//index.php 
require_once 'includes/global.inc.php';
$page = "study.php"
?>
<html>
	<head>
		<title>Обучение | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<?php $user = unserialize($_SESSION['user']); ?>
		
		<?php if(isset($_SESSION['logged_in'])) : ?>
			
		<?php else : ?>
			
		<?php endif; ?>
		<div class="alert alert-danger" role="alert">
				  <strong>Ошибка безопасности #004</strong><br>
				  <p>Страница находится на доработке. Скоро мы ее опубликуем.</p>
				  <hr>
				  <small>
 				 <p class="mb-0">There was study.php GET request, but page is not active.<br>
				 Был совершен GET запрос на страницу обучения (study.php), но страница не активна.</p></small>
				</div>	
		</main>
		</body>
</html>