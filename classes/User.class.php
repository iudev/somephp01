<?php
//User.class.php

require_once 'DB.class.php';

class User {
public $id;
public $username;
public $hashedPassword;
public $email;
public $joinDate;
public $familiya;
public $imya;
public $otchestvo;
public $data_rojd;
public $school;
public $classs;
public $coach;
public $division;
public $points;
public $acc_type;
public $admin;

//Конструктор вызывается при создании нового объекта
//Takes an associative array with the DB row as an argument.
function __construct($data) {
$this->id = (isset($data['id'])) ? $data['id'] : "";
$this->username = (isset($data['username'])) ? $data['username'] : "";
$this->hashedPassword = (isset($data['password'])) ? $data['password'] : "";
$this->email = (isset($data['email'])) ? $data['email'] : "";
$this->joinDate = (isset($data['join_date'])) ? $data['join_date'] : "";
$this->familiya = (isset($data['familiya'])) ? $data['familiya'] : "";
$this->imya = (isset($data['imya'])) ? $data['imya'] : "";
$this->otchestvo = (isset($data['otchestvo'])) ? $data['otchestvo'] : "";
$this->data_rojd = (isset($data['data_rojd'])) ? $data['data_rojd'] : "";
$this->school = (isset($data['school'])) ? $data['school'] : "";
$this->classs = (isset($data['classs'])) ? $data['classs'] : "";
$this->coach = (isset($data['coach'])) ? $data['coach'] : "";
$this->division = (isset($data['division'])) ? $data['division'] : "";
$this->points = (isset($data['points'])) ? $data['points'] : "";
$this->acc_type = (isset($data['acc_type'])) ? $data['acc_type'] : "";
$this->admin = (isset($data['admin'])) ? $data['admin'] : "";
}

public function save($isNewUser = false) {
//create a new database object.
$db = new DB();

//if the user is already registered and we're
//just updating their info.
if(!$isNewUser) {
//set the data array
$data = array(
"username" => "'$this->username'",
"password" => "'$this->hashedPassword'",
"email" => "'$this->email'",
"familiya" => "'$this->familiya'",
"imya" => "'$this->imya'",
"otchestvo" => "'$this->otchestvo'",
"data_rojd" => "'$this->data_rojd'",
"school" => "'$this->school'",
"classs" => "'$this->classs'",
"coach" => "'$this->coach'",
"division" => "'$this->division'",
"points" => "'$this->points'",
"acc_type" => "'$this->acc_type'",
"admin" => "'$this->admin'"
);

//update the row in the database
$db->update($data, 'users', 'id = '.$this->id);
}else {
//if the user is being registered for the first time.
$data = array(
"username" => "'$this->username'",
"password" => "'$this->hashedPassword'",
"email" => "'$this->email'",
"join_date" => "'".date("Y-m-d H:i:s",time())."'",
"familiya" => "'$this->familiya'",
"imya" => "'$this->imya'",
"otchestvo" => "'$this->otchestvo'",
"data_rojd" => "'$this->data_rojd'",
"school" => "'$this->school'",
"classs" => "'$this->classs'",
"coach" => "'$this->coach'",
"division" => "'4'",
"points" => "'0'",
"acc_type" => "'$this->acc_type'",
"admin" => "'0'"
);
$this->id = $db->insert($data, 'users');
$this->joinDate = time();
}
return true;
}
}
?>