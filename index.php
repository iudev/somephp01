<?php
//index.php 
require_once 'includes/global.inc.php';
$page = "index.php"
?>
<html>
	<head>
		<title>Главная | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<center>
		<?php $user = unserialize($_SESSION['user']); ?>
		<?php if(isset($_SESSION['logged_in'])) : ?>
			Вы вошли в систему.<br><br>
		<?php else : ?>
			Вы не вошли в систему.<br><br>
		<?php endif; ?>
		<div class="alert alert-warning" role="alert">
		  <h4 class="alert-heading">Бета-версия сайта.</h4>
		  <p>В данный момент сайт находится в стадии бета-разработки. Пожалуйста, сообщайте администрации обо всех возникающих ошибках.</p>
		  <hr>
		  <p class="mb-0"><small>Даниил Бондарь / admin / 03.10.2018 14:21</small></p>
		</div>
		<div class="alert alert-success" role="alert">
		  <h4 class="alert-heading">Запуск сайта ШАРП.</h4>
		  <p>Запущен информационный сайт проекта ШАРП.<br>На сайте Вы можете пройти регистрацию как участник проекта, посмотреть свои баллы за участие в мероприятиях проекта,
		  получить доступ к системе обучения и записаться на предстоящие соревнования.</p>
		  <hr>
		  <p class="mb-0"><small>Даниил Бондарь / admin / 03.10.2018 14:23</small></p>
		</div>
		</center>
		</main>
		</body>
</html>