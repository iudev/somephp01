<?php
//welcome.php
require_once 'includes/global.inc.php';
$page = "welcome.php";

//проверить вошел ли пользователь
if(!isset($_SESSION['logged_in'])) {
header("Location: login.php");
}
//взять объект user из сессии
$user = unserialize($_SESSION['user']);

?>

<html>
<head>
<title>Успешная регистрация | ШАРП</title>
<?php require_once 'includes/bootstrap.inc.php'; ?>
</head>
<body>
<?php require_once 'includes/header.inc.php'; ?>
<div class="alert alert-success" role="alert">
  <strong>Регистрация пройдена успешно</strong><br>
  <a href="index.php">Нажмите, чтобы перейти на главную.</a>
</div>
</body>
</html>