<?php
//index.php 
require_once 'includes/global.inc.php';
$page = "about.php"
?>
<html>
	<head>
		<title>О проекте | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<?php $user = unserialize($_SESSION['user']); ?>
		About
		<?php if(isset($_SESSION['logged_in'])) : ?>
			
		<?php else : ?>
			
		<?php endif; 
		<center>
		<h1>Проект "ШАРП"</h1>

		<h4>Республиканский проект по привлечению и развития интереса детей к программированию и ИТ-сфере.</h4>

		Находимся в режиме развития в Удмуртской республике.<br><br>

		Организации - авторы проекта:<br>
		МБОУ ИТ-лицей №24 г. Ижевск<br>
		АУ УР "РЦИ и ОКО", Удмуртская республика (Детский технопарк "Кванториум")<br><br>

		Проводим соревнования по программированию, системному администрированию, веб-разработке, а также создаем онлайн-курсы по всем направлениям.
		</main>
		</center>
		</body>
</html>